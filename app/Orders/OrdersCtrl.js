'use strict';

angular
  .module('CafeApp')
  .controller('OrdersCtrl', function(AuthService, UserService, OrderService, TimeParsingService, SocketService, $state, $sessionStorage) {
    let vm = this;
    vm.loading = true;
    vm.getTime = TimeParsingService;

    if (!AuthService.userLoggedIn()) {
      return $state.go('login');
    } else {
      vm.user = $sessionStorage.user;
    }

    vm.logout = function() {
      AuthService.logout();
      return $state.go('login');
    };

    vm.topUp = function() {
      UserService.topUp({
          _id: vm.user._id,
          sum: 100
        })
        .$promise
        .then((user) => {
          $sessionStorage.user.balance = user.balance;
          vm.user = $sessionStorage.user;
        });
    };

    function getOrders() {
      OrderService.query({
          user: vm.user._id
        }).$promise
        .then((orders) => {
          vm.orders = orders;
          vm.loading = false;
        });
    }

    getOrders();

    SocketService.emit('userJoin', {
      userId: vm.user._id
    });

    SocketService.on('orderUpdate', function(order) {
      vm.orders = vm.orders.map(function(item) {
        if (item._id == order._id)
          item = order;
        return item;
      });
    });

    SocketService.on('orderDestroyed', function(order) {
      vm.orders = vm.orders.filter((item) => {
        return item._id != order._id;
      });
    });

    SocketService.on('refund', function(balance) {
      if ($sessionStorage.user) {
        $sessionStorage.user.balance = balance;
      }
    });


  });
