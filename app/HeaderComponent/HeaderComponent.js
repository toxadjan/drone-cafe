'use strict';

angular
  .module('CafeApp')
  .component('headerComponent', {
    templateUrl: 'HeaderComponent/HeaderComponent.html',
    controller: function($scope, $state, AuthService) {
      let vm = this;

      if (!AuthService.userLoggedIn()) {
        vm.userLoggedIn = false;
      } else {
        vm.userLoggedIn = true;
      }

      $scope.$on('login', function(event, data) {
        vm.userLoggedIn = true;
      });

      $scope.$on('logout', function(event, data) {
        vm.userLoggedIn = false;
      });

      vm.logout = function() {
        AuthService.logout();
        return $state.go('login');
      };

    }
  });
