'use strict';

angular
  .module('CafeApp')
  .controller('KitchenCtrl', function(OrderService, TimeParsingService, SocketService, $q) {
    let vm = this;
    vm.loading = true;
    vm.getTime = TimeParsingService;

    let ordersListRequest = OrderService.query({
      status: 'Ordered'
    }).$promise;

    let cookingListRequest = OrderService.query({
      status: 'Cooking'
    }).$promise;

    $q.all([ordersListRequest, cookingListRequest])
      .then((response) => {
        vm.ordersList = response[0];
        vm.cookingList = response[1];
        vm.loading = false;
      });


    /**
     * Вызывает метод update у сервиса OrderService, передав в него id заказа и новый статус заказа Cooking.
     * В случае ответа с удачно разрешенным промисом:
     * - удаляет 1 элемент из массива ordersList, начиная с индекса $index;
     * - добавляет в массив cookingList обновленный заказ, переданный в ответе промиса;
     * Если промис отклонен:
     * - выводит объект с информацией об ошибке в консоль
     *
     * @param  {Object} order - Объект с данными о заказе
     * @param  {number} $index - индекс заказа в массиве заказов
     * @return {Object} - Обновленный заказ
     */
    vm.startCooking = function(order, $index) {
      OrderService.update({
          _id: order._id,
          status: 'Cooking'
        })
        .$promise
        .then((res) => {
          if (res.error) {
            console.log(res.error);
            return;
          }

          vm.ordersList.splice($index, 1);
          vm.cookingList.push(res);
        });
    };

    /**
     * Вызывает метод update у сервиса OrderService, передав в него id заказа и новый статус заказа Delivering.
     * В ответ получает промис.
     * Если промис отклонен:
     * - выводит объект с информацией об ошибке в консоль;
     * В случае ответа с удачно разрешенным промисом:
     * - удаляет 1 элемент из массива cookingList, начиная с индекса $index;
     * - вызывает метод deliver у сервиса OrderService, передав в него id заказа;
     *
     * @param  {Object} order - Объект с данными о заказе
     * @param  {number} $index - индекс заказа в массиве заказов
     * @return {Object} - Обновленный заказ
     */
    vm.finishCooking = function(order, $index) {
      OrderService.update({
          _id: order._id,
          status: 'Delivering'
        })
        .$promise
        .then((res) => {
          if (res.error) {
            console.log(res.error);
            return;
          }

          vm.cookingList.splice($index, 1);
          OrderService.deliver({
            _id: order._id
          });
        });
    };

    SocketService.on('newOrder', function(data) {
      vm.ordersList.push(data);
    });


  });
