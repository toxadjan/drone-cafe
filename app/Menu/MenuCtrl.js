'use strict';

angular
  .module('CafeApp')
  .controller('MenuCtrl', function(AuthService, DishService, UserService, OrderService, $scope, $http, $state, $sessionStorage) {
    let vm = this;

    if (!AuthService.userLoggedIn()) {
      return $state.go('login');
    } else {
      vm.user = $sessionStorage.user;
    }

    vm.loading = true;
    vm.limit = 6;
    vm.pageSize = 5;
    vm.pageStart = 0;

    vm.getMenu = function(page) {
      vm.currentPage = page || 1;
      vm.loading = true;

      DishService.query({
        limit: vm.limit,
        page: page
      }).$promise.then((res) => {
        vm.dishes = res.dishes;
        vm.totalPages = res.totalPages;
        vm.loading = false;
      });
    };

    vm.joinList = function(list) {
      return list.join(", ");
    };

    vm.topUp = function(amount) {
      UserService.topUp({
          _id: vm.user._id,
          sum: amount
        })
        .$promise
        .then((user) => {
          $sessionStorage.user.balance = user.balance;
        });
    };

    vm.purchase = function(dish) {
      OrderService.save({
          user: vm.user._id,
          dish: dish._id,
          status: 'Ordered',
          price: dish.price
        }).$promise
        .then((res) => {
          $sessionStorage.user.balance = res.user.balance;
          $state.go('orders');
        });
    };

    vm.getMenu();

  });
