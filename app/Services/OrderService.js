angular
  .module('CafeApp')
  .factory('OrderService', function($resource) {
    return $resource('api/orders/:_id', {
      _id: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      deliver: {
        method: 'PUT',
        url: 'api/orders/:_id/deliver'
      }
    });
  });
