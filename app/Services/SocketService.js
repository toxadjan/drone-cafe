angular
  .module('CafeApp')
  .factory('SocketService', function(socketFactory) {
    return socketFactory({
      // ioSocket: io.connect('http://127.0.0.1:3000/')
      ioSocket: io.connect('https://netology-nodejs-diploma.herokuapp.com/')
    });
  });
