angular
  .module('CafeApp')
  .factory('AuthService', function(UserService, $rootScope, $sessionStorage) {
    let auth = {};

    /**
     * Вызывает метод get у сервиса UserService, передав в него объект с данными пользователя из формы.
     * В результате получает промис.
     * Если промис разрешен успешно:
     * - генерирует событие login для $rootScope;
     * - если в ответе промиса в теле объекта существует поле user._id - записывает этот объект в $sessionStorage
     * - если поле user._id пустое - вызывает метод save у сервиса UserService, тем самым создавая документ с новым пользователем в БД.
     * Вовзращает объект с данными о пользователе.
     *
     * @param  {Object} userData - данные пользователя
     * @return {Object} user - полученный из БД объект пользователя
     */
    auth.login = function(userData) {
      return UserService.get({
        email: userData.email
      }).$promise.then((user) => {
        $rootScope.$broadcast('login');
        if (user._id) {
          $sessionStorage.user = user;
          return user;
        }

        return UserService.save(userData).$promise.then((user) => {
          $sessionStorage.user = user;
          return user;
        });
      });
    };

    auth.logout = function() {
      $rootScope.$broadcast('logout');
      delete $sessionStorage.user;
    };

    auth.userLoggedIn = function() {
      return !!$sessionStorage.user;
    };

    return auth;

  });
