angular
  .module('CafeApp')
  .factory('TimeParsingService', function() {
    return (date) => {
      const time = new Date(date);
      let dd = time.getDate();
      let mm = time.getMonth() + 1; //January is 0!
      let yyyy = time.getFullYear();
      let hours = time.getHours();
      let minutes = time.getMinutes();

      if (dd < 10) {
        dd = '0' + dd;
      }

      if (mm < 10) {
        mm = '0' + mm;
      }

      if (hours <= 9) {
        hours = '0' + hours;
      }
      if (minutes <= 9) {
        minutes = '0' + minutes;
      }
      return `${dd}.${mm}.${yyyy} ${hours}:${minutes}`;
    };
  });
