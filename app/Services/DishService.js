angular
  .module('CafeApp')
  .factory('DishService', function($resource) {
    return $resource('api/dishes/:_id', {
      _id: '@_id'
    }, {
      query: {
        transformResponse: function(responseData) {
          return angular.fromJson(responseData);
        }
      }
    });
  });
