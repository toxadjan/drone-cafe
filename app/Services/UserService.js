angular
  .module('CafeApp')
  .factory('UserService', function($resource) {
    return $resource('api/users/:_id', {
      _id: '@_id'
    }, {
      topUp: {
        method: 'PUT'
      }
    });
  });
