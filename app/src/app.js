'use strict';

angular.module('CafeApp', [
    'ui.router',
    'ngMessages',
    'ngMaterial',
    'ngResource',
    'ngStorage',
    'btford.socket-io'
  ])
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state({
        name: 'login',
        url: '/login',
        component: 'authComponent'
      })
      .state({
        name: 'kitchen',
        url: '/kitchen',
        templateUrl: 'Kitchen/Kitchen.html',
        controller: 'KitchenCtrl as vm'
      })
      .state({
        name: 'orders',
        url: '/',
        templateUrl: 'Orders/Orders.html',
        controller: 'OrdersCtrl as vm'
      })
      .state({
        name: 'menu',
        url: '/menu',
        templateUrl: 'Menu/Menu.html',
        controller: 'MenuCtrl as vm'
      });

    $urlRouterProvider.otherwise('/');

  })
  .config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('purple')
      .accentPalette('deep-purple');
  });


$(document).ready(function() {
  $('.sidenav').sidenav();
});
