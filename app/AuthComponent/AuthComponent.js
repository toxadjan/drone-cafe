'use strict';

angular
  .module('CafeApp')
  .component('authComponent', {
    templateUrl: 'AuthComponent/AuthComponent.html',
    controller: function(AuthService, $state) {
      let vm = this;

      vm.login = function(userData) {
        AuthService.login(userData)
          .then((user) => {
            $state.go('orders');
          });
      };
    }
  });
