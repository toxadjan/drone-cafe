const mongoose = require('mongoose');
const dbUrl = 'mongodb://localhost:27017/drone-cafe';

const menu = require('../app/src/menu');
const Dish = require('./models/DishModel');

mongoose.connect(dbUrl, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
}, err => {
  if (err) return console.log(err);
});

for (var i = 0; i < menu.length; i++) {
  new Dish({
      title: menu[i].title,
      image: menu[i].image,
      rating: menu[i].rating,
      ingredients: menu[i].ingredients,
      price: menu[i].price
    })
    .save((err) => {
      if (err) {
        console.log(err);
      }
    });
}
