'use strict';

const order = require('../controllers/OrderController');

module.exports = function(app, socket) {
  app.route('/api/orders/')
    .get(order.getOrders)
    .post(order.createOrder(socket));

  app.route('/api/orders/:id')
    .put(order.updateOrderStatus(socket))
    .delete(order.destroyOrder);

  app.route('/api/orders/:id/deliver')
    .put(order.deliverOrder(socket));
};
