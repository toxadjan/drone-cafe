'use strict';

const dish = require('../controllers/DishController');

module.exports = function(app) {
  app.route('/api/dishes')
    .get(dish.getDishes);
};
