'use strict';

const user = require('../controllers/UserController');

module.exports = function(app) {
  app.route('/api/users/')
    .get(user.findUser)
    .post(user.createUser);

  app.route('/api/users/:id')
    .put(user.topUp);
};
