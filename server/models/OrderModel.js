'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = mongoose.Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  dish: {
    type: Schema.Types.ObjectId,
    ref: 'Dish'
  },
  status: {
    type: String,
    enum: ['Ordered', 'Cooking', 'Delivering', 'Failed', 'Served']
  }
}, {
  timestamps: true
});

module.exports = mongoose.model('Order', OrderSchema);
