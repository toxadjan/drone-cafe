const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');

const mongoose = require('mongoose');
// const dbUrl = 'mongodb://netology:nodejs1@ds211096.mlab.com:11096/heroku_26xhn8ck';
const dbUrl = process.env.MONGO_URL || 'mongodb://localhost:27017/drone-cafe';

let server = app.listen(port, () => {
  console.log(`Listening on port ${port}!`);
  mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  }, err => {
    if (err) return console.log(err);
    console.log('Connected to DB');
  });
});

const io = require('socket.io').listen(server);
let socket = io;

io.on('connection', function(socket) {
  console.log('a user connected');
  socket.on('userJoin', function (data) {
    socket.join(data.userId);
  });
  socket.on('disconnect', function() {
    console.log('user disconnected');
  });
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  'extended': true
}));
app.use(express.static('./app'));

require('./api/UserApi')(app);
require('./api/DishApi')(app);
require('./api/OrderApi')(app, socket);

app.use(function(req, res) {
  res.status(404).send('404 Not Found');
});

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

module.exports = app;
