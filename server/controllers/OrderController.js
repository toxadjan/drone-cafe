'use strict';

const Order = require('../models/OrderModel');
const User = require('../controllers/UserController');
const Drone = require('netology-fake-drone-api');

/**
 * Создает новый документ с заказом пользователя в базе данных.
 * Обновляет текущий баланс пользователя в базе данных, вычитывая стоимость заказанного блюда.
 * Генерирует событие newOrder для объекта socket.
 * Возвращает обновленный объект с телом заказа.
 *
 * @param  {Object} socket - Объект сокета
 * @param  {Object} req.body - Объект с телом заказа
 * @return {Object} Обновленный объект заказа
 */
function createOrder(socket) {
  return function(req, res) {
    let order = new Order(req.body);

    order.save()
      .then((order) => {
        return Order.populate(order, 'dish');
      })
      .then((order) => {
        return User.updateBalance(order.user, -order.dish.price);
      })
      .then((user) => {
        order.user = user;
        socket.emit('newOrder', order);
        res.json(order);
      })
      .catch((error) => {
        return res.status(500).send({
          error: error.message
        });
      });
  };
}

/**
 * Возвращает из базы данных объект со списком заказов пользователя.
 *
 * @param  {String} req.query - ID пользователя
 * @return {Object} - Список заказов пользователя
 */
function getOrders(req, res) {
  Order.find(req.query).populate('dish').exec()
    .then((orders) => {
      res.json(orders);
    })
    .catch((error) => {
      return res.status(500).send({
        error: error.message
      });
    });
}

/**
 * Находит документ заказа в базе данных и обновляет статус заказа.
 * Генерирует событие orderUpdate в личный канал пользователя для объекта socket.
 * Проверяет значение статуса заказа.
 * Если статус Failed или Served - вызывает функцию destroyOrder
 * Возвращает обновленный объект заказа.
 *
 * @param  {Object} socket - Объект сокета
 * @param  {String} req.params.id - ID заказа
 * @return {Object} - Обновленный заказ
 */
function updateOrderStatus(socket) {
  return function(req, res) {

    Order.findOneAndUpdate({
        '_id': req.params.id
      }, {
        $set: {
          status: req.body.status
        }
      }, {
        new: true
      })
      .populate('dish')
      .exec()
      .then((order) => {
        if (!order) {
          return res.send({
            error: 'No order has been found (updateOrderStatus)'
          });
        } else {
          socket.to(order.user).emit('orderUpdate', order);
          if (req.body.status == 'Failed' || req.body.status == 'Served') {
            destroyOrder(order, socket);
          }
          res.json(order);
        }
      })
      .catch((error) => {
        return res.status(500).send({
          error: error.message
        });
      });
  };
}

/**
 * Находит документ заказа в базе данных.
 * Вызывает метод deliver у объекта Drone (netology-fake-drone-api).
 * В ответ получает промис.
 * Если промис разрешен успешно:
 * - вызывает функцию updateOrderStatus, передав в нее новый статус заказа Served и объект сокета;
 * Если промис отклонен:
 * - вызывает метод updateBalance у объекта User, передав в него стоимость блюда в заказе и ID пользователя;
 * - генерирует событие refund в личный канал пользователя для объекта socket;
 * - вызывает функцию updateOrderStatus, передав в нее новое значение статуса заказа Failed и объект сокета;
 *
 * @param  {Object} socket - Объект сокета
 * @param  {String} req.params.id - ID заказа
 * @return {Object} -
 */
function deliverOrder(socket) {
  return function(req, res) {

    Order.findById(req.params.id).populate('dish').populate('user').exec()
      .then((order) => {
        Drone.deliver(order.user, order.dish)
          .then(() => {
            req.body.status = 'Served';
            updateOrderStatus(socket)(req, res);
          })
          .catch(() => {
            User.updateBalance(order.user._id, order.dish.price)
              .then((user) => {
                socket.to(user._id).emit('refund', user.balance);
                req.body.status = 'Failed';
                updateOrderStatus(socket)(req, res);
              });
          });
      })
      .catch((error) => {
        return res.status(500).send({
          error: error.message
        });
      });
  };
}

/**
 * Возвращает функцию setTimeout, которая выполнится через 120 секунд.
 * В теле этой функции удаляет документ заказа из базы данных по ID заказа.
 * Генерирует событие orderDestroyed в личный канал пользователя для объекта socket;
 *
 * @param  {Object} socket - Объект сокета
 * @param  {Object} order - Объект заказа
 * @return {Function} - Функция setTimeout
 */
function destroyOrder(order, socket) {
  setTimeout(() => {
    Order.deleteOne({
        _id: order._id
      })
      .exec()
      .then(() => {
        socket.to(order.user).emit('orderDestroyed', order);
      });

  }, 120000);
}

module.exports = {
  createOrder,
  getOrders,
  updateOrderStatus,
  deliverOrder,
  destroyOrder
};
