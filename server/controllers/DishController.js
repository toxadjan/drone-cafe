'use strict';

const Dish = require('../models/DishModel');

/**
 * Возвращает объект меню со списком блюд и общим количеством страниц меню.
 * Сортирует выдачу блюд в зависимости от переданных параметров в запросе.
 *
 * @param  {number} req.query.limit - Лимит на вывод количества блюд
 * @param  {number} req.query.page - Текущая страница пользователя в меню
 * @return {Object} Новый объект меню
 */
 function getDishes(req, res) {
  let limit = parseInt(req.query.limit);
  let page = parseInt(req.query.page) || 1;
  let offset = (page - 1) * limit;
  let totalPages = 0;

  Dish.countDocuments()
    .then((totalDishes) => {
      totalPages = Math.ceil(totalDishes / limit);
      return Dish.find().limit(limit).skip(offset).exec();
    })
    .then((dishes) => {
      res.json({
        dishes: dishes,
        totalPages: totalPages
      });
    })
    .catch((error) => {
      return res.status(500).send({
        error: error.message
      });
    });
}

module.exports = {
  getDishes
};
