'use strict';

const User = require('../models/UserModel');

/**
 * Создает новый документ пользователя в базе данных.
 * Возвращает полученный из базы данных объект пользователя.
 *
 * @param  {Object} req.body - Объект с имходными данными пользователя
 * @return {Object} Новый объект пользователя
 */
function createUser(req, res) {
  let user = new User(req.body);

  user.save()
    .then(function() {
      res.json(user);
    })
    .catch(function(error) {
      return res.status(500).send({
        error: error.message
      });
    });
}

/**
 * Ищет в базе данных пользователя по заданному параметру.
 * Если пользователь не найден - возвращает объект с полем error
 * Если пользователь найден - возвращает объект с данными пользователя.
 *
 * @param  {String} req.query.email - email пользователя
 * @return {Object} - Объект с результатами поиска
 */
function findUser(req, res) {
  let email = req.query.email;

  User.findOne({
      email: email
    })
    .exec()
    .then((user) => {
      if (!user) {
        return res.send({
          error: `Cannot find user with email ${email}`
        });
      } else {
        res.json(user);
      }
    })
    .catch((error) => {
      return res.status(500).send({
        error: error.message
      });
    });
}

/**
 * Вызывает функцию updateBalance, которая обновляет баланс пользователя.
 * Возвращает обновленный объект пользователя из базы данных.
 * В случае наличия ошибки в теле ответа - возвращает объект с информацией об ошибке.
 *
 * @param  {String} req.params.id - id пользователя
 * @param  {number} req.body.sum - сумма пополнения
 * @return {Object} - Обновленный объект пользователи
 */
function topUp(req, res) {
  let id = req.params.id;
  let sum = req.body.sum;

  updateBalance(id, sum)
    .then((user) => {
      res.json(user);
    })
    .catch((error) => {
      return res.status(500).send({
        error: error.message
      });
    });
}

/**
 * Находит пользователя в базе данных по ID.
 * Прибавляет к полю balance значение sum.
 * Записывает обновленный документ пользователя в БД.
 *
 * @param  {String} id - id пользователя
 * @param  {number} sum - сумма пополнения
 * @return {Function} - метод findOneAndUpdate у объекта User
 */
function updateBalance(id, sum) {
  return User.findOneAndUpdate({
    '_id': id
  }, {
    $inc: {
      'balance': sum
    }
  }, {
    new: true
  }).exec();
}

module.exports = {
  createUser,
  findUser,
  topUp,
  updateBalance
};
