const KitchenPage = require('./page-objects/kitchen-page');
const OrdersPage = require('./page-objects/orders-page');
const LoginPage = require('./page-objects/login-page');

describe('KITCHEN PAGE: ', () => {
  let kitchenPage = {};
  let ordersPage = {};
  let loginPage = {};

  beforeAll(() => {
    loginPage = new LoginPage();
    ordersPage = new OrdersPage();
    loginPage.get();
    loginPage.login();

    ordersPage = new OrdersPage();
    ordersPage.topUp();
    browser.wait(() => {
      return ordersPage.isLoaded();
    });
  });

  beforeEach(() => {
    ordersPage.getMenu();
    let dish = ordersPage.getDish();

    ordersPage.getDishName(dish)
      .then((dishName) => {
        ordersPage.buyDish(dish);
      });
    kitchenPage = new KitchenPage();
    kitchenPage.get();
  });

  describe('when start cooking button is clicked', () => {
    it('dish should be removed from the orders list', () => {
      let ordersList = kitchenPage.getOrdersList();
      kitchenPage.getDishName(ordersList)
        .then((dishName) => {
          let countBefore = kitchenPage.getDishCount(ordersList, dishName);
          kitchenPage.getActionBtn(ordersList).click();
          let countAfter = kitchenPage.getDishCount(ordersList, dishName);

          expect(countAfter).toBeLessThan(countBefore);
        });
    });

    it('dish should be moved to the cooking list', () => {
      let ordersList = kitchenPage.getOrdersList();
      let cookingList = kitchenPage.getCookingList();
      kitchenPage.getDishName(ordersList)
        .then((dishName) => {
          let countBefore = kitchenPage.getDishCount(cookingList, dishName);
          kitchenPage.getActionBtn(ordersList).click();
          let countAfter = kitchenPage.getDishCount(cookingList, dishName);

          expect(countAfter).toBeGreaterThan(countBefore);
        });
    });
  });

  describe('when end cooking button is clicked', () => {
    it('dish should be removed from the cooking list', () => {
      let cookingList = kitchenPage.getCookingList();
      kitchenPage.getDishName(cookingList)
        .then((dishName) => {
          let countBefore = kitchenPage.getDishCount(cookingList, dishName);
          kitchenPage.getActionBtn(cookingList).click();
          let countAfter = kitchenPage.getDishCount(cookingList, dishName);

          expect(countAfter).toBeLessThan(countBefore);
        });
    });
  });

  afterAll(() => {
    ordersPage = new OrdersPage();
    ordersPage.get();
    ordersPage.logout();
  });
});
