const LoginPage = require('./page-objects/login-page');
let homepage = 'http://localhost:3000/#!/';

describe('LOGIN PAGE', () => {
  let loginPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    loginPage.get();
  });

  describe('when user is not logged in', () => {
    it('should redirect to /login when trying to access homepage', () => {
      browser.get(homepage);
      expect(browser.getCurrentUrl()).toMatch(loginPage.getUrl());
    });
  });

  describe('when user email is incorrect', function() {
    it('should display error message', function() {
      loginPage.setEmail('incorrect email format');
      expect(element(by.css('.alert-danger')).isDisplayed()).toBe(true);
    });

    it('login button should be disabled', function() {
      let btn = loginPage.getLoginBtn();
      loginPage.setName('John');
      loginPage.setEmail('incorrect email format');
      expect(btn.isEnabled()).toBe(false);
    });
  });

  describe('when username and email are correct', function() {
    it('login button should be enabled', function() {
      let btn = loginPage.getLoginBtn();
      loginPage.setName('John');
      loginPage.setEmail('john@gg.com');
      expect(btn.isEnabled()).toBe(true);
    });

    it('should redirect to home page', function() {
      loginPage.login();
      expect(browser.getCurrentUrl()).toMatch(homepage);
    });
  });
});
