let OrdersPage = function() {
  let self = this;
  let url = 'http://localhost:3000/#!/';
  let menuUrl = 'http://localhost:3000/#!/menu';
  let balance = element(by.css('.user-balance'));
  let topUpButton = element(by.css('.topup-btn'));
  let logoutButton = element(by.css('.logout-btn'));
  let newOrderBtn = element(by.css('.order-btn'));

  self.get = function() {
    browser.get(url);
  };

  self.getMenu = function() {
    browser.get(menuUrl);
  };

  self.getMenuUrl = function() {
    return menuUrl;
  };

  self.isLoaded = function() {
    return browser.getCurrentUrl().then((currentUrl) => {
      return currentUrl == url;
    });
  };

  self.getBalance = function() {
    return self.toNumber(balance.getText());
  };

  self.toNumber = function(promise) {
    return promise.then((string) => {
      return parseFloat(string.replace(/,|\$/g, ''));
    });
  };

  self.topUp = function() {
    topUpButton.click();
  };

  self.logout = function() {
    logoutButton.click();
  };

  self.makeNewOrder = function() {
    newOrderBtn.click();
  };

  self.getDish = function() {
    return element(by.repeater('dish in vm.dishes').row(0));
  };

  self.buyDish = function(dish) {
    dish.element(by.css('.buy-btn')).click();
  };

  self.getDishName = function(dish) {
    return dish.element(by.css('.card-title')).getText();
  };

  self.getDishPrice = function(dish) {
    return self.toNumber(dish.element(by.css('.dish-price')).getText());
  };

  self.findOrdersByName = function(dishName) {
    return element.all(by.repeater('order in vm.orders')).filter(function(order, index) {
      return order.all(by.css("h3")).getText().then(function(name) {
        return name == dishName;
      });
    });
  };

  self.countOrders = function(dishName) {
    return self.findOrdersByName(dishName).count();
  };
};

module.exports = OrdersPage;
