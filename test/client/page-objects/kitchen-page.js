let KitchenPage = function() {
  let self = this;
  let url = 'http://localhost:3000/#!/kitchen';

  self.get = function() {
    browser.get(url);
  };

  self.getOrdersList = function() {
    return element.all(by.repeater('order in vm.ordersList'));
  };

  self.getCookingList = function() {
    return element.all(by.repeater('order in vm.cookingList'));
  };

  self.getDishName = function(list) {
    return list.last().element(by.css('h3')).getText();
  };

  self.getActionBtn = function(list) {
    return list.last().element(by.css('.md-button'));
  };

  self.getDishCount = (list, dishName) => {
    return list.filter((row) => {
      return row.element(by.css('h3')).getText().then((res) => {
        return res === dishName;
      });
    }).count();
  };
};

module.exports = KitchenPage;
