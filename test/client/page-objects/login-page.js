let LoginPage = function() {
  let self = this;
  let url = 'http://localhost:3000/#!/login';
  let userName = 'John';
  let userEmail = 'john@gmail.com';
  let nameInput = element(by.model('user.name'));
  let emailInput = element(by.model('user.email'));
  let loginButton = element(by.css('.md-button'));

  self.get = function() {
    browser.get(url);
  };

  self.getUrl = function () {
    return url;
  };

  self.setName = function(value) {
    nameInput.sendKeys(value);
  };

  self.setEmail = function(value) {
    emailInput.sendKeys(value);
  };

  self.login = function() {
    self.setName(userName);
    self.setEmail(userEmail);
    loginButton.click();
  };

  self.getLoginBtn = function() {
    return loginButton;
  };

};

module.exports = LoginPage;
