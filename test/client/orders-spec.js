const OrdersPage = require('./page-objects/orders-page');
const LoginPage = require('./page-objects/login-page');

describe('ORDERS PAGE:', () => {
  let ordersPage = {};
  let loginPage = {};

  beforeAll(() => {
    loginPage = new LoginPage();
    ordersPage = new OrdersPage();
    loginPage.get();
    loginPage.login();

    browser.wait(() => {
      return ordersPage.isLoaded();
    });
  });

  beforeEach(() => {
    ordersPage.get();
  });

  describe('when click topUp button', () => {
    it('user balance should be increased by 100', () => {
      let balanceBefore = ordersPage.getBalance();
      ordersPage.topUp();
      let balanceAfter = ordersPage.getBalance();
      Promise.all([balanceBefore, balanceAfter])
        .then((results) => {
          expect(results[0] + 100).toEqual(results[1]);
        });
    });
  });

  describe('when click button "make new order"', () => {
    it('should redirect to menu page', () => {
      ordersPage.makeNewOrder();
      expect(browser.getCurrentUrl()).toMatch(ordersPage.getMenuUrl());
    });
  });

  describe('when purchase dish', () => {
    it('should display purchased dish in orders list', () => {
      ordersPage.getMenu();
      let dish = ordersPage.getDish();

      ordersPage.getDishName(dish)
        .then((dishName) => {
          ordersPage.buyDish(dish);
          expect(ordersPage.countOrders(dishName)).toBeGreaterThan(0);
        });
    });

    it('user balance should be reduced by dish price', () => {
      let balanceBefore = ordersPage.getBalance();
      ordersPage.getMenu();
      let dish = ordersPage.getDish();
      let dishPrice = ordersPage.getDishPrice(dish);
      ordersPage.buyDish(dish);

      browser.wait(() => {
        return ordersPage.isLoaded();
      });

      let balanceAfter = ordersPage.getBalance();
      Promise.all([balanceBefore, dishPrice, balanceAfter])
        .then((results) => {
          expect(results[0] - results[1]).toEqual(results[2]);
        });
    });

    it('order status should update to Ordered', () => {
      ordersPage.getMenu();
      let dish = ordersPage.getDish();

      ordersPage.getDishName(dish)
        .then((dishName) => {
          ordersPage.buyDish(dish);
          let orderStatus = ordersPage.findOrdersByName(dishName).last().element(by.css('.Ordered')).getText();
          expect(orderStatus).toEqual('ORDERED');
        });
    });
  });

  describe('when click logout button', () => {
    it('should redirect to login page', () => {
      ordersPage.logout();
      expect(browser.getCurrentUrl()).toMatch(loginPage.getUrl());
    });
  });

});
