const chai = require('chai');
const supertest = require('supertest');
const assert = chai.assert;
const User = require('../../server/models/UserModel');
const Dish = require('../../server/models/DishModel');
const Order = require('../../server/models/OrderModel');

let testUser = {
  name: 'John',
  email: 'john@gg.com',
  balance: 100
};

let testOrder = {
  status: 'Ordered'
};

let server;

describe('RESTFUL ORDER CONTROLLER', () => {

  before((done) => {
    require('../../server/server');

    let user = new User(testUser);
    let order = new Order(testOrder);

    Promise.all([
      user.save(),
      Dish.findOne().exec()
    ]).then((result) => {
      testUser = result[0];
      testOrder.user = result[0].id;
      testOrder.dish = result[1].id;
    }).then(() => {
      setTimeout(() => {
        server = supertest.agent('http://localhost:3000');
        done();
      }, 1000);
    });
  });

  after((done) => {
    deleteUser(testUser._id)
      .then(() => {
        deleteOrder(testOrder._id)
          .then(() => {
            done();
          });
      });
  });

  it('POST: should create order in db and return correct order object with status 200', (done) => {
    server
      .post('/api/orders/')
      .send(testOrder)
      .end((error, res) => {
        assert.property(res.body, '_id');
        testOrder._id = res.body._id;
        assert.equal(res.status, 200);
        assert.nestedProperty(res.body, 'user._id');
        assert.deepNestedPropertyVal(res.body, 'dish._id', testOrder.dish);
        assert.deepNestedPropertyVal(res.body, 'user._id', testOrder.user);
        assert.isUndefined(res.body.error);
        done();
      });
  });

  it('GET: should return order list with status 200', (done) => {
    server
      .get('/api/orders/')
      .end((error, res) => {
        assert.equal(res.status, 200);
        assert.isArray(res.body);
        assert.isUndefined(res.body.error);
        done();
      });
  });

  it('PUT: should update order status and return correct object with status 200', (done) => {
    server
      .put("/api/orders/" + testOrder._id)
      .send({
        status: "Cooking"
      })
      .end((error, res) => {
        assert.equal(res.status, 200);
        assert.propertyVal(res.body, 'status', 'Cooking');
        assert.isUndefined(res.body.error);
        done();
      });
  });
});

function deleteOrder(id) {
  return Order.deleteOne({
    _id: id
  }).exec();
}

function deleteUser(id) {
  return User.deleteOne({
    _id: id
  }).exec();
}
