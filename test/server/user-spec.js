const chai = require('chai');
const supertest = require('supertest');
const assert = chai.assert;
const User = require('../../server/models/UserModel');

let testUser = {
  name: 'John',
  email: 'john@gg.com',
  balance: 100
};

describe('RESTFUL USER CONTROLLER', () => {
  let server;

  before((done) => {
    require('../../server/server');
    setTimeout(() => {
      server = supertest.agent('http://localhost:3000');
      done();
    }, 1000);
  });

  afterEach((done) => {
    deleteUser().then(() => {
      done();
    });
  });

  describe('when user doesnt exist', () => {

    it('POST: should create user in db and return correct user object with status 200', (done) => {
      server
        .post('/api/users/')
        .send(testUser)
        .end((error, res) => {
          assert.equal(res.status, 200);
          assert.property(res.body, '_id');
          assert.propertyVal(res.body, 'email', testUser.email);
          assert.isUndefined(res.body.error);
          done();
        });
    });

  });


  describe('when user already exists', () => {
    beforeEach((done) => {
      createUser().then(() => {
        done();
      });
    });

    it('POST: should return status 500 with error message when trying to create another user with the same email', (done) => {
      server
        .post('/api/users/')
        .send(testUser)
        .end((error, res) => {
          assert.equal(res.status, 500);
          assert.isDefined(res.body.error);
          done();
        });
    });


    it('GET: should find user by email and return correct user object with status 200', (done) => {
      server
        .get('/api/users/?email=' + testUser.email)
        .end((error, res) => {
          assert.equal(res.status, 200);
          assert.property(res.body, '_id');
          assert.propertyVal(res.body, 'email', testUser.email);
          assert.isUndefined(res.body.error);
          done();
        });
    });

    it('GET: should update user balance and return correct user object with status 200', (done) => {
      server
        .get('/api/users/?email=' + testUser.email)
        .end((error, res) => {
          let id = res.body._id;
          server.put('/api/users/' + id)
            .send({
              sum: 50
            })
            .end((error, res) => {
              assert.equal(res.status, 200);
              assert.equal(res.body.balance, 150);
              assert.isUndefined(res.body.error);
              done();
            });
        });
    });

  });

});

function createUser() {
  let user = new User(testUser);
  return user.save();
}

function deleteUser() {
  return User.deleteOne({
    email: testUser.email
  }).exec();
}
