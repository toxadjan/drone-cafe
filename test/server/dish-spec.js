const chai = require('chai');
const supertest = require('supertest');
const assert = chai.assert;

describe('RESTFUL DISH CONTROLLER', () => {
  let server;

  before((done) => {
    require('../../server/server');
    setTimeout(() => {
      server = supertest.agent('http://localhost:3000');
      done();
    }, 1000);
  });

  it('GET: should return correct dish list object with status 200', (done) => {
    server
      .get('/api/dishes/')
      .end((error, res) => {
        assert.equal(res.status, 200);
        assert.property(res.body, 'dishes');
        assert.property(res.body, 'totalPages');
        assert.isUndefined(res.body.error);
        done();
      });
  });
});
