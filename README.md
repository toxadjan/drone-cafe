# Drone Cafe
Дипломный проект онлайн университета Нетология в рамках курса NodeJS, AngularJS и MongoDB: разработка полноценных веб-приложений

Условия задачи: https://bitbucket.org/netology-university/drone-cafe/


## Demo
[https://netology-nodejs-diploma.herokuapp.com/](https://netology-nodejs-diploma.herokuapp.com/)

- [Интерфейс клиента](https://netology-nodejs-diploma.herokuapp.com/)
- [Интерфейс повара](https://netology-nodejs-diploma.herokuapp.com/#!/kitchen)

## Описание

### Основные модули
* Сервер реализован на Node.js с использованием express.js
* Взаимодействие клиента и сервера в реальном времени реализовано с использованием socket.io
* Интерфейс системы построен на фреймворке AngularJS с использованием модулей:
  * angular-socket-io - для работы с сокетами
  * angular-material - для работы с интерфейсом приложения
  * angular-local-storage - для работы с cookie
  * angular-resource - для работы с RESTful API
  * angular-ui-router - для работы с роутами
  * angular-messages - для работы с валидацией
* Для хранения данных использована база данных MongoDB с библиотекой Mongoose
* RESTful API построено на NodeJS с использованием фреймворка ExpressJS
* В качестве CSS фреймворков использованы Materialize CSS и AngularJS Material
* Дизайн приложения адаптирован под мобильные устройства


### Структура проекта
```
app		
  |---	AuthComponent
      |---	AuthComponent.html
      |---	AuthComponent.js
  |---	HeaderComponent
      |---	HeaderComponent.html
      |---	HeaderComponent.js
  |---	Kitchen
      |---	Kitchen.html
      |---	KitchenCtrl.js
  |---	Menu
      |---	Menu.html
      |---	MenuCtrl.js
  |---	Orders
      |---	Orders.html
      |---	OrdersCtrl.js
  |---	Services
      |---	AuthService.js
      |---	DishService.js
      |---	OrderService.js
      |---	SocketService.js
      |---	TimeParserService.js
      |---	UserService.js
  |---	src
      |---	css
      |---	js
      |---	app.js
      |---	menu.json
  |---	index.html
  |---	node_modules
server		
  |---	api
      |---	DishApi.js
      |---	OrderApi.js
      |---	UserApi.js
  |---	controllers
      |---	DishController.js
      |---	OrderController.js
      |---	UserController.js
  |---	models
      |---	DishModel.js
      |---	OrderModel.js
      |---	UserModel.js
  |---	menuImport.js
  |---	server.js
test		
  |---	client
  |---	server

```
#### Клиентская часть
Реализована с использованием фреймворка AngularJs. Представляет собой Single Page Application. Структура клиентской части с компонентами и сервисами расположена в ```/app/```.

* ```/``` - главная страница клиента с личной информацией и списком заказов
* ```/menu``` - страница со списком доступных к заказу блюд
* ```/kitchen``` - интерфейс повара со списком заказанных и готовящихся блюд
* ```/login``` - страница авторизации

В случае переходов на страницы ```/``` и ```/menu``` система проверяет авторизацию пользователя. Если пользователь не авторизован, приложение отправляет на ```/login```. Для входа в интерфейс повара авторизация не требуется.

### Серверная часть
* ```/server/server.js``` - основной файл приложения, осуществляет подключение  необходимых модулей и запуск сервера
* ```/server/api/``` - роуты для api запросов, обрабатываемых сервером
* ```/server/controllers/``` - модули, отвечающие за бизнес логику приложения
* ```/server/models/``` - модели БД

Реализовано 3 модели БД:
- ```User``` пользователи кафе
- ```Order``` заказы пользователей
- ```Dish``` доступные для заказа блюда

### Тестирование
Файлы со сценариями тестов располагаются в папке test.

Тесты сервера (REST API и Socket) реализованы с использованием библиотек:
-	Mocha
-	Chai
-	Supertest

Тесты интерфейса реализованы с использованием:
- Protractor
- Jasmine

После запуска тестов клиентской части, отчет со скриншотами и результатами тестов будет расположен в ```/test/client/tmp/reports/```

### Cкрипты для работы с приложением
- Перед запуском приложения необходимо выполнить команду ```npm install```
- Для запуска сервера ```npm run start``` Приложение станет доступно по адресу http://127.0.0.1:3000/#!/
- Для запуска тестов клиентской части ```npm run protractor```
- Для запуска тестов серверной части ```npm run test```

###Примечания
Для корректной работы сокетов в приложении с локальной версией сервера, необходимо в файле ```app/Services/SocketService.js``` изменить URL в строке ```ioSocket: io.connect('https://netology-nodejs-diploma.herokuapp.com/')``` на ```http://127.0.0.1:3000/```

Для использования локальной версии mongodb в файле ```server/server.js``` следует заменить значение URL в строке ```const dbUrl = '*';``` на ```const dbUrl = 'mongodb://localhost:27017/drone-cafe';``` Далее следует выполнить импорт блюд меню в локальную базу данных (должен быть запущен сервер MongoDB) - ```npm run import```
